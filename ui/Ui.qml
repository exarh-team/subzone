import QtQuick 2.4

UiForm {
    header.width: (wnd.width < maxWndWidth+20) ? wnd.width - 20 : maxWndWidth

    leftBtn.anchors.leftMargin: (wnd.width < maxWndWidth+40) ? 20 : wnd.width/2-maxWndWidth/2

    buildBtn.anchors.rightMargin: (wnd.width < maxWndWidth+40) ? 20 : wnd.width/2-maxWndWidth/2

    footer.width: (wnd.width < maxWndWidth) ? wnd.width : maxWndWidth

    buildMenu.width: (wnd.width < maxWndWidth + 40) ? wnd.width - 40 : maxWndWidth
}
