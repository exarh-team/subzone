import QtQuick 2.4
import QtGraphicalEffects 1.0

Column {
    id: leftBtn
    property alias btnShellGPS: btnShellGPS
    property alias btnGPS: btnGPS
    anchors.left: parent.left
    anchors.leftMargin: (wnd.width < maxWndWidth + 40) ? 20 : wnd.width / 2 - maxWndWidth / 2
    anchors.top: parent.top
    anchors.topMargin: 70
    spacing: 10

    Rectangle {
        id: btnShellGPS
        width: 52
        height: 52
        color: "#fff"
        radius: 52

        ColorOverlay {
            width: gpsFixedIcon.width
            height: gpsFixedIcon.height
            source: gpsFixedIcon
            color: "#fff"
            visible: gps_locked
            anchors.centerIn: parent
        }
        ColorOverlay {
            width: gpsOffIcon.width
            height: gpsOffIcon.height
            source: gpsOffIcon
            color: "#ccc"
            visible: !gps_locked
            anchors.centerIn: parent
        }

        MouseArea {
            id: btnGPS
            anchors.fill: parent
        }
    }
}
