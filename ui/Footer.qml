import QtQuick 2.4

import "qrc:/cyberman.js" as Cyberman

FooterForm {
    build.onPressed: {
        buildMenu.state = (buildMenu.state === "show") ? "hide" : "show"
        if (wnd.buildMode === true) {
            wnd.buildMode = false
            buildBtn.state = "hide"
            Cyberman.changeBuildMode()
            Cyberman.build_cancelSpawn()
        }
    }
}
