import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

ColumnLayout {
    id: header

    width: 400
    height: 50
    property alias energy_line_ghost: energy_line_ghost
    property alias energy_text: energy_text
    property alias lvl: lvl
    property alias login: login
    property alias energy_line: energy_line
    property alias clan: clan
    spacing: 0

    Item {
        Layout.preferredHeight: 42
        Layout.fillWidth: true

        Text {
            id: login
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.horizontalCenter
            anchors.rightMargin: 62
            color: "#fff"
            font.pointSize: 12
        }
        Text {
            id: lvl
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.horizontalCenter
            anchors.rightMargin: 30
            color: "#fff49e"
            font.weight: Font.DemiBold
            font.pointSize: 12
        }
        Rectangle {
            id: rectangle
            width: 34
            height: 34
            color: "#999"
            anchors.centerIn: parent
        }
        Text {
            id: clan
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.horizontalCenter
            anchors.leftMargin: 30
            color: "#fff"
            font.pointSize: 12
        }
    }

    Rectangle {
        height: 8
        color: "#323232"
        border.width: 1
        border.color: "#424242"
        Layout.fillWidth: true

        Item {
            id: energy_line_ghost
            anchors.rightMargin: 1
            anchors.leftMargin: 1
            anchors.bottomMargin: 1
            anchors.topMargin: 1
            anchors.fill: parent
        }

        Rectangle {
            id: energy_line
            color: "#3d9113"
            border.width: 1
            border.color: "#323232"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 1
            anchors.top: parent.top
            anchors.topMargin: 1
            anchors.left: parent.left
            anchors.leftMargin: 1
        }

        Text {
            id: energy_text
            anchors.fill: parent
            color: "#ffffff"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            opacity: 0.3
            font.pixelSize: 10
        }

        DropShadow {
            anchors.fill: parent
            horizontalOffset: 0
            verticalOffset: 0
            radius: 2.0
            samples: 5
            opacity: 0.8
            color: "#000"
            source: energy_text
        }
    }
}
