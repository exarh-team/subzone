import QtQuick 2.4

Rectangle {
    property alias buildMenu: buildMenu

    id: buildMenu

    width: 400
    height: 200
    color: "#000000"
    radius: 2
    property alias factory: factory
    property alias cannon: cannon
    state: "hide"
    states: [
        State {
            name: "show"
        },
        State {
            name: "hide"
            PropertyChanges {
                target: buildMenu
                anchors.topMargin: -500 - 60
                anchors.bottomMargin: 500 + 100
                opacity: 0
            }
        }
    ]

    Grid {
        anchors.fill: parent
        columns: 5
        Item {
            width: parent.width / parent.columns
            height: parent.width / parent.columns

            Image {
                anchors.centerIn: parent
                width: parent.width - parent.width / 5
                height: width
                source: 'qrc:/img/cannon.png'
            }

            MouseArea {
                id: cannon
                anchors.fill: parent
            }
        }
        Item {
            width: parent.width / parent.columns
            height: parent.width / parent.columns

            Image {
                anchors.centerIn: parent
                width: parent.width - parent.width / 5
                height: width
                source: 'qrc:/img/factory.png'
            }

            MouseArea {
                id: factory
                anchors.fill: parent
            }
        }
    }
}
