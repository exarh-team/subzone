import QtQuick 2.4
import QtPositioning 5.8

LeftBtnForm {
    btnShellGPS.color: Qt.rgba(1, 1, 1, 0.2)
    btnGPS.onClicked: {
        gps_locked = !gps_locked;

        if (gps_locked) {
            if (!isNaN(src.position.coordinate.longitude)) {
                mapBase.center = src.position.coordinate
            } else {
                mapBase.center = QtPositioning.coordinate(50.38894, 36.87731)
            }
        }
    }
}
