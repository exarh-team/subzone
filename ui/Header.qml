import QtQuick 2.4

HeaderForm {
    function update() {
        energy_line.width = energy_line_ghost.width*(actorData.energy_current/actorData.energy_max);
        energy_text.text = actorData.energy_current+"/"+actorData.energy_max;
    }

    login.text: actorData.login
    lvl.text: "[L"+actorData.lvl+"]"
    clan.text: actorData.clan

    Behavior on energy_line.width {
        NumberAnimation { duration: 500 }
    }

    Component.onCompleted: {
        update();
    }
}
