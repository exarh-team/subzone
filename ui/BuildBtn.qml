import QtQuick 2.4

import "qrc:/cyberman.js" as Cyberman

BuildBtnForm {
    buildShellOk.color: Qt.rgba(1, 1, 1, 0.2)
    buildShellCancel.color: Qt.rgba(1, 1, 1, 0.2)

    buildBtnOk.onClicked: {
        if (imagegrid_ok) {
            buildMenu.state = "hide"
            buildBtn.state = "hide"
            wnd.buildMode = false;
            Cyberman.changeBuildMode();
            Cyberman.build_goSpawn();
        }
    }
    buildBtnCancel.onClicked: {
        buildMenu.state = "hide"
        buildBtn.state = "hide"
        wnd.buildMode = false
        Cyberman.changeBuildMode()
        Cyberman.build_cancelSpawn()
    }
}
