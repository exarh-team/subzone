import QtQuick 2.4
import QtGraphicalEffects 1.0

Column {
    id: footer

    width: 400
    height: 80
    property alias build: build

    Row {
        height: 40
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        Rectangle {
            color: "#333"
            border.width: 1
            border.color: "#444"
            width: parent.width / 5 - 4
            height: parent.height

            ColorOverlay {
                width: buildIcon.width
                height: buildIcon.height
                source: buildIcon
                color: "#fff"
                anchors.centerIn: parent
                visible: true
            }

            MouseArea {
                id: build
                anchors.fill: parent
            }
        }

        Rectangle {
            color: "#333"
            border.width: 1
            border.color: "#444"
            width: parent.width / 5 - 4
            height: parent.height
        }

        Rectangle {
            color: "#333"
            border.width: 1
            border.color: "#444"
            width: parent.width / 5 - 4
            height: parent.height
        }

        Rectangle {
            color: "#333"
            border.width: 1
            border.color: "#444"
            width: parent.width / 5 - 4
            height: parent.height
        }

        Item {
            width: 16
            height: parent.height
        }

        Rectangle {
            color: "#112244"
            width: parent.width / 5
            height: parent.height
        }
    }

    Rectangle {
        height: 40
        color: "#112244"
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
    }
}
