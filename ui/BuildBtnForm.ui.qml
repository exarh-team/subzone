import QtQuick 2.4
import QtGraphicalEffects 1.0

Row {
    id: buildBtn
    property alias buildBtnCancel: buildBtnCancel
    property alias buildBtnOk: buildBtnOk
    property alias buildShellOk: buildShellOk
    property alias buildShellCancel: buildShellCancel
    spacing: 10
    state: "hide"
    states: [
        State {
            name: "show"
        },
        State {
            name: "hide"
            PropertyChanges {
                target: buildBtn
                opacity: 0
            }
        }
    ]

    //    transitions: [
    //        Transition {
    //            PropertyAnimation {
    //                target: ok_cancel
    //                properties: "opacity"
    //                duration: animationDuration
    //                easing.type: Easing.InOutQuad
    //            }
    //        }
    //    ]
    Rectangle {
        id: buildShellOk
        width: 52
        height: 52
        color: "#fff"
        opacity: (imagegrid_ok) ? 1 : 0.2
        radius: 2
        ColorOverlay {
            width: okIcon.width
            height: okIcon.height
            source: okIcon
            color: "#cfc"
            visible: true
            anchors.centerIn: parent
        }

        MouseArea {
            id: buildBtnOk
            anchors.fill: parent
        }
    }
    Rectangle {
        id: buildShellCancel
        width: 52
        height: 52
        color: "#fff"
        radius: 2
        ColorOverlay {
            width: cancelIcon.width
            height: cancelIcon.height
            source: cancelIcon
            color: "#fcc"
            visible: true
            anchors.centerIn: parent
        }

        MouseArea {
            id: buildBtnCancel
            anchors.fill: parent
        }
    }
}
