import QtQuick 2.4
import QtPositioning 5.8

import "qrc:/cyberman.js" as Cyberman

BuildMenuForm {
    buildMenu.color: Qt.rgba(0, 0, 0, 0.5)

    buildMenu.transitions: [
        Transition {
            PropertyAnimation {
                target: buildMenu
                properties: "anchors.topMargin, anchors.bottomMargin, opacity"
                duration: animationDuration
                easing.type: Easing.InOutQuad
            }
        }
    ]

    cannon.onPressed: {
        buildMenu.state = "hide"
        buildBtn.state = "show"
        wnd.buildMode = true
        wnd.buildType = "cannon"
        Cyberman.changeBuildMode()
    }

    factory.onPressed: {
        buildMenu.state = "hide"
        buildBtn.state = "show"
        wnd.buildMode = true
        wnd.buildType = "factory"
        Cyberman.changeBuildMode()
    }
}
