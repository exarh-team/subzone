import QtQuick 2.4
import QtQuick.Window 2.2
import QtLocation 5.9
import QtPositioning 5.8
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

Item {
    id: ui
    property alias header: header
    property alias leftBtn: leftBtn
    property alias buildBtn: buildBtn
    property alias footer: footer
    property alias buildMenu: buildMenu

    Header {
        id: header
        anchors.horizontalCenter: parent.horizontalCenter
    }

    LeftBtn {
        id: leftBtn
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 70
    }

    BuildBtn {
        id: buildBtn
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 100
    }

    Footer {
        id: footer
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }

    BuildMenu {
        id: buildMenu
        anchors.top: parent.top
        anchors.topMargin: 70
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 100
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
