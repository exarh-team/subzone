
// https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#ECMAScript_.28JavaScript.2FActionScript.2C_etc..29
function long2tile(lon,zoom) { return (Math.floor((lon+180)/360*Math.pow(2,zoom))); }
function lat2tile(lat,zoom)  { return (Math.floor((1-Math.log(Math.tan(lat*Math.PI/180) + 1/Math.cos(lat*Math.PI/180))/Math.PI)/2 *Math.pow(2,zoom))); }

function tile2long(x,z) { return (x/Math.pow(2,z)*360-180); }
function tile2lat(y,z) { var n=Math.PI-2*Math.PI*y/Math.pow(2,z); return (180/Math.PI*Math.atan(0.5*(Math.exp(n)-Math.exp(-n)))); }

function deg2num(lat, lon, zoom) { return [lat2tile(lat,zoom), long2tile(lon,zoom)]; }
function num2deg(xtile, ytile, zoom) { return [tile2lat(xtile,zoom), tile2long(ytile,zoom)] }
// привязка координат к сетке
function deg2grid(lat, lon, zoom, half) {
    var num = deg2num(lat, lon, zoom);
    var plus = (half === true) ? 0.5 : 0;
    return num2deg(num[0]+plus, num[1]+plus, zoom);
}
