import QtQuick 2.7
import QtLocation 5.9
import QtPositioning 5.8

MapQuickItem {
    property alias coordinateAnimation: coordinateAnimation
    property int animationTime: 500

    id: container
    zoomLevel: 19
    property var tile24
    anchorPoint.x: image.width/2
    anchorPoint.y: image.height/2

    Behavior on coordinate {
        id: coordinateAnimation
        enabled: false
        CoordinateAnimation { duration: animationTime }
    }

    Behavior on opacity {
        NumberAnimation {
            duration: animationTime/2
        }
    }

    sourceItem: Image{
        id: image
        source: 'qrc:/img/energy.png'
    }

    Timer {
        id: opacityTimer
        interval: animationTime/2
        running: false
        onTriggered: container.opacity = 0
    }

    Timer {
        id: destroyTimer
        interval: animationTime
        running: false
        onTriggered: container.destroy()
    }

    function destroyContainer() {
        opacityTimer.running = true
        destroyTimer.running = true
    }
}
