Qt.include("qrc:/geo.js");
Qt.include("qrc:/logic/build.js")
Qt.include("qrc:/logic/actor.js")
Qt.include("qrc:/logic/energy.js")

var countEnergy = 0;
var energyComponent = Qt.createComponent("qrc:/component/Energy.qml");
var gridComponent = Qt.createComponent("qrc:/Grid.qml");
var factoryComponent = Qt.createComponent("qrc:/Factory.qml");
var cannonComponent = Qt.createComponent("qrc:/Cannon.qml");

var gridUnit;

function spawnEnergy(lat, lon, num) {

    var energyUnit = energyComponent.createObject(
        wnd, {
            "coordinate": QtPositioning.coordinate(lat, lon),
            "tile24": num,
            "coordinateAnimation.enabled": true
        }
    );

    energyItems.push(energyUnit);
    mapOverlay.addMapItem(energyUnit);



//    var circle = Qt.createQmlObject('import QtLocation 5.9; MapCircle {}', wnd);
//    circle.center = QtPositioning.coordinate(lat, lon);
//    circle.radius = 30.0;
//    circle.color = 'green';
//    circle.opacity = 0.1;
//    circle.border.width = 1;

//    energyItems.push(circle);
//    mapOverlay.addMapItem(circle);

}

function spawnNet(lat, lon) {

    var pos = deg2grid(lat, lon, 20);

    grid.coordinate = QtPositioning.coordinate(pos[0], pos[1])
    grid.visible = true;

    //energyItems.push(gridUnit);
    mapOverlay.addMapItem(gridUnit);
}

function spawnFactory(lat, lon) {
    var num = deg2num(lat, lon, 20);
    var pos = num2deg(num[0], num[1], 20);

    factoryUnit = factoryComponent.createObject(
        wnd, {
            "coordinate": QtPositioning.coordinate(pos[0], pos[1])
        }
    );
    mapOverlay.addMapItem(factoryUnit);
}

function spawnCannon(lat, lon) {
    var num = deg2num(lat, lon, 20);
    var pos = num2deg(num[0], num[1], 20);

    cannonUnit = cannonComponent.createObject(
        wnd, {
            "coordinate": QtPositioning.coordinate(pos[0], pos[1])
        }
    );
    mapOverlay.addMapItem(cannonUnit);
}

function init() {



//    var circle = Qt.createQmlObject("import QtQuick 2.7; import QtLocation 5.9; MapQuickItem { zoomLevel: 17; anchorPoint.x: image.width/2; anchorPoint.y: image.height/2; sourceItem: Image{id: image; source: 'net.png'} }", wnd);
//    circle.coordinate = QtPositioning.coordinate(simpleLat, simpleLon);
//    circle.opacity = 1;

//    energyItems.push(circle);
//    mapOverlay.addMapItem(circle);

//    spawnEnergy(src.position.coordinate.latitude+0.1, src.position.coordinate.longitude+0.1);
//    spawnEnergy(src.position.coordinate.latitude+0.1, src.position.coordinate.longitude-0.1);
//    spawnEnergy(src.position.coordinate.latitude-0.1, src.position.coordinate.longitude+0.1);
//    spawnEnergy(src.position.coordinate.latitude-0.1, src.position.coordinate.longitude-0.1);

//    var count = 0;
//    for (var lat=50.39047;lat<=50.58;lat+=0.0005) {
//        for (var lon=36.87717;lon<=36.58;lon+=0.001) {
//            spawnEnergy(lat, lon);
//            count++;
//        }
//    }
//    console.log(count);

//    var circle = Qt.createQmlObject('import QtLocation 5.9; MapCircle {}', wnd)
//    circle.center = QtPositioning.coordinate(50.3904, 36.87717)
//    circle.radius = 5000.0
//    circle.color = 'green'
//    circle.opacity = 0.1
//    circle.border.width = 3
//    mapOverlay.addMapItem(circle)

//    var circle = Qt.createQmlObject('import QtLocation 5.9; MapCircle {}', wnd)
//    circle.center = QtPositioning.coordinate(50.3904, 36.87717)
//    circle.radius = 500000.0
//    circle.color = 'green'
//    circle.opacity = 0.1
//    circle.border.width = 3
//    mapOverlay.addMapItem(circle)

}

function changePos() {

    var lat = mapBase.center.latitude;
    var lon = mapBase.center.longitude;

    if (isNaN(lat)) return;

//    energyItems.forEach(function(element, index) {
//        console.log(element);
//        if (
//            (element.center.latitude - lat)*(element.center.latitude - lat) +
//            (element.center.longitude - lon)*(element.center.longitude - lon) < 0.000001
//            ) {
//            console.log("del");
//            countEnergy += 1;
//            //countEnergyText.text = countEnergy;

//            energyItems.splice(index, 1);
//            mapOverlay.removeMapItem(element);
//        }
//    });

    console.log(lat);
}

function changeBuildMode() {
    energy_check_and_eat();
    if (wnd.buildMode === true) {
        spawnNet(mapBase.center.latitude, mapBase.center.longitude);
        if ((wnd.buildType === "factory")) {
            spawnFactory(mapBase.center.latitude, mapBase.center.longitude);
        }
        if ((wnd.buildType === "cannon")) {
            spawnCannon(mapBase.center.latitude, mapBase.center.longitude);
        }
        build_check_possibility(mapBase.center.latitude, mapBase.center.longitude);
    } else {
        grid.visible = false;
    }
}
