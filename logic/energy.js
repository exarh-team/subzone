function arrayClean(myArray) {
    var newArray = [];
    var idx = 0;
    for (var item in myArray) {
        newArray[idx++] = myArray[item];
    }
    return newArray;
}

function enegry_demo_spawn(lat, lon) {
    var num = deg2num(lat, lon, 24);
    var ll;
    for (var x=-70;x<=70;x+=10) {
        for (var y=-70;y<=70;y+=10) {
            if (x*x+y*y >= 9000) continue;
            if ((x == 0)&&(y == 0)) continue;

            ll = num2deg(num[0]+x, num[1]+y, 24);
            spawnEnergy(ll[0], ll[1], [num[0]+x, num[1]+y]);
        }
    }
}

function energy_check_and_eat() {
    var num_actor = deg2num(actorPosition.latitude, actorPosition.longitude, 24);
    var num_energy;
    energyItems.forEach(function(element, index) {
        num_energy = element.tile24;
        if (
                (
                    (num_actor[0]-num_energy[0])*(num_actor[0]-num_energy[0])
                    +
                    (num_actor[1]-num_energy[1])*(num_actor[1]-num_energy[1])
                ) < 3000) {
            element.coordinate = actorPosition;
            element.destroyContainer();
            delete energyItems[index];

            if (actorData.energy_current < actorData.energy_max) {
                actorData.energy_current += 10;
            }
            if (actorData.energy_current > actorData.energy_max) {
                actorData.energy_current = actorData.energy_max
            }

            ui_energy.update();
        }
    });

//    console.log("actorData");
//    console.log(actorData);
//    console.log("energy_current");
//    console.log(actorData.energy_current);
//    console.log("energy_max");
//    console.log(actorData.energy_max);
//    if (actorData.energy_current > actorData.energy_max) {
//        actorData.energy_current = actorData.energy_max;
//    }

    energyItems = arrayClean(energyItems);
}
