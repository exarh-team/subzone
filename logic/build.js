function _build_tile_plus_size(num, size_x, size_y) {
    var arr = [];
    for (var x = num[0]-size_x/2; x <= num[0]+size_x/2; x+=0.5) {
        for (var y = num[1]-size_y/2; y <= num[1]+size_y/2; y+=0.5) {
            arr.push([x, y]);
        }
    }
    return arr;
}

function build_check_possibility(lat, lon) {
    var pos, size_x, size_y;
    var num = deg2num(lat, lon, 20);

    if (wnd.buildType === "factory") {
        pos = num2deg(num[0]-factoryUnit.size_x/2+0.5, num[1]-factoryUnit.size_y/2+0.5, 20);
        factoryUnit.coordinate = QtPositioning.coordinate(pos[0], pos[1]);
        size_x = factoryUnit.size_x;
        size_y = factoryUnit.size_y;
        factoryUnit.tiles20 = _build_tile_plus_size(num, size_x, size_y);
    }
    if (wnd.buildType === "cannon") {
        pos = num2deg(num[0]-cannonUnit.size_x/2+0.5, num[1]-cannonUnit.size_y/2+0.5, 20);
        cannonUnit.coordinate = QtPositioning.coordinate(pos[0], pos[1]);
        size_x = cannonUnit.size_x;
        size_y = cannonUnit.size_y;
        cannonUnit.tiles20 = _build_tile_plus_size(num, size_x, size_y);
    }

    imagegrid_ok = true;
    items.forEach(function(element, index) {

        for (var x = num[0]-size_x/2; x <= num[0]+size_x/2; x+=0.5) {
            for (var y = num[1]-size_y/2; y <= num[1]+size_y/2; y+=0.5) {

                element.tiles20.forEach(function(el, i) {

                    if ((el[0] === x)&&(el[1] === y)) {
                        imagegrid_ok = false;
                    }

                });



            }
        }


    });
}



function build_goSpawn() {
    if (wnd.buildType === "factory") {
        items.push(factoryUnit);
    }
    if (wnd.buildType === "cannon") {
        items.push(cannonUnit);
    }
}


function build_cancelSpawn() {
    if (wnd.buildType === "factory") {
        factoryUnit.destroy();
    }
    if (wnd.buildType === "cannon") {
        cannonUnit.destroy();
    }
}
