function getMaxEnergyByLvl(lvl) {
    if ((lvl<1)||(lvl>9)) {
        return 0;
    } else {
        // 1 - 3000
        // 2 - 4500
        // 3 - 6000
        // 4 - 7500
        // 5 - 9000
        // 6 - 10500
        // 7 - 12000
        // 8 - 13500
        // 9 - 15000

        return (lvl*1.5*1000+1500);
    }
}

function modelActor(raw) {
    return {
        login: (("login" in raw) ? raw.login : 'Anonymous'),
        email: (("email" in raw) ? raw.email : ''),
        clan: (("clan" in raw) ? raw.clan : qsTr("[LONER]")),
        lvl: (("lvl" in raw) ? raw.lvl : 1),
        energy_current: (("energy" in raw) ? raw.energy : 0),
        energy_max: getMaxEnergyByLvl((("lvl" in raw) ? raw.lvl : 1))
    }
}

function actorInit() {
    var data = {
        login: "modos189",
        lvl: 1,
        energy: 1000
    }

    actorData = modelActor(data)
}
