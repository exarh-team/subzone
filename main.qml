import QtQuick 2.0
import QtQuick.Window 2.2
import QtLocation 5.9
import QtPositioning 5.8
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtSensors 5.0

import "qrc:/component"
import "qrc:/cyberman.js" as Cyberman
import "qrc:/ui"

Window {
    id: wnd
    color: '#000'

    property int maxWndWidth: 500
    property int animationDuration: 200
    property bool buildMode: false
    property string buildType: ""
    property bool imagegrid_ok: true
    property bool gps_locked: true

    // хранилище всех зданий
    property var items: []
    // хранилище всей энергии
    property var energyItems: []
    // для строительства новых зданий
    property var cannonUnit
    property var factoryUnit

    // данные о нашем игроке
    property var actorData
    property var actorPosition: (!isNaN(src.position.coordinate.longitude)) ? src.position.coordinate : QtPositioning.coordinate(50.38894, 36.87731)

    property alias ui_energy: ui.header
    width: 400
    height: 640

    visibility: Window.AutomaticVisibility
    visible: true

    Image {
        id: buildIcon
        visible: false
        source: Qt.resolvedUrl("qrc:/icons/ic_build_black_24px.svg")
        width: 24
        height: 24
        sourceSize: Qt.size(width, height)
    }

    Image {
        id: okIcon
        visible: false
        source: Qt.resolvedUrl("qrc:/icons/ic_done_black_24px.svg")
        width: 32
        height: 32
        sourceSize: Qt.size(width, height)
    }

    Image {
        id: cancelIcon
        visible: false
        source: Qt.resolvedUrl("qrc:/icons/ic_cancel_black_24px.svg")
        width: 32
        height: 32
        sourceSize: Qt.size(width, height)
    }

    Image {
        id: gpsFixedIcon
        visible: false
        source: Qt.resolvedUrl("qrc:/icons/ic_gps_fixed_black_24px.svg")
        width: 32
        height: 32
        sourceSize: Qt.size(width, height)
    }

    Image {
        id: gpsOffIcon
        visible: false
        source: Qt.resolvedUrl("qrc:/icons/ic_gps_off_black_24px.svg")
        width: 32
        height: 32
        sourceSize: Qt.size(width, height)
    }

    PositionSource {
        id: src
        updateInterval: 1000
        active: true

        onPositionChanged: {
            var coord = src.position.coordinate;

            // Cyberman.changePos();
            console.log("Coordinate:", coord.longitude, coord.latitude);

            if (gps_locked) {
                mapBase.center = src.position.coordinate
            }

            Cyberman.energy_check_and_eat();
        }

        Component.onCompleted: {
            Cyberman.init();
            Cyberman.energy_check_and_eat();
        }
    }

    Plugin {
        id: mapPlugin
        name: "mapboxgl"

        PluginParameter {
            name: "mapboxgl.access_token"
            value: "pk.eyJ1IjoibW9kb3MxODkiLCJhIjoiY2o1ZTBqdHM4MGtiYzMzbGR4M3hxMmg0eiJ9.LPaDCwSr5_9z69RS45E0Fw"
        }
        PluginParameter {
            name: "mapboxgl.mapping.use_fbo"
            value: false
        }
        PluginParameter {
            name: "mapboxgl.mapping.additional_style_urls"
            value: "mapbox://styles/modos189/cj6jnmw806e3p2rn9jynq17e0"
        }
    }

    Map
    {
        id: mapBase

        anchors.fill: parent
        plugin: mapPlugin
        tilt: 30

        gesture.enabled: true
        gesture.acceptedGestures: (buildMode) ? (MapGestureArea.TiltGesture) : (MapGestureArea.PinchGesture | MapGestureArea.PanGesture);
        gesture.onPanStarted: {
            Cyberman.changePos();
            gps_locked = false;
        }

        center: actorPosition
        zoomLevel: 17
    }

//    Map {
//        id: mapBase
//        anchors.fill: parent

//        plugin: Plugin {
//            name: "mapboxgl"
//            PluginParameter {
//                name: "mapboxgl.access_token"
//                value: "pk.eyJ1IjoibW9kb3MxODkiLCJhIjoiY2o1ZTBqdHM4MGtiYzMzbGR4M3hxMmg0eiJ9.LPaDCwSr5_9z69RS45E0Fw"
//            }
//            PluginParameter {
//                name: "mapboxgl.mapping.additional_style_urls"
//                value: "mapbox://styles/mapbox/dark-v9"
//            }
//        }

//        center: QtPositioning.coordinate(50.57195, 36.57219)
//        zoomLevel: 17
//    }


    Map {
        id: mapOverlay
        anchors.fill: parent
        plugin: Plugin { name: "itemsoverlay" }
        gesture.enabled: false
        center: mapBase.center
        color: 'transparent' // Necessary to make this map transparent
        minimumFieldOfView: mapBase.minimumFieldOfView
        maximumFieldOfView: mapBase.maximumFieldOfView
        minimumTilt: mapBase.minimumTilt
        maximumTilt: mapBase.maximumTilt
        minimumZoomLevel: mapBase.minimumZoomLevel
        maximumZoomLevel: mapBase.maximumZoomLevel
        zoomLevel: mapBase.zoomLevel
        tilt: mapBase.tilt;
        bearing: mapBase.bearing
        fieldOfView: mapBase.fieldOfView
        z: mapBase.z + 1

        MapQuickItem {
            id: radius
            zoomLevel: 19
            coordinate: actorPosition
            anchorPoint.x: image.width/2
            anchorPoint.y: image.height/2
            sourceItem: Image{
                id: image;
                source: 'qrc:/img/radius.png'
            }
            z: 10
        }

        MapQuickItem {
            id: actor_shadow
            zoomLevel: 19
            coordinate: actorPosition
            anchorPoint.x: actor_shadow_image.width/2
            anchorPoint.y: actor_shadow_image.height/2
            sourceItem: Image{
                id: actor_shadow_image;
                width: 57
                height: 87
                source: 'qrc:/img/actor_shadow.png'
                transform: Rotation {
                    id: actorShadowRotation
                    origin.x: 57/2
                    origin.y: 87/2
                    angle: 0
                    Behavior on angle {
                        SpringAnimation { spring: 2; damping: 0.3; modulus: 360 }
                    }
                }
            }
            opacity: 0.5
            z: 11
        }

        MapQuickItem {
            id: actor_arrow
            zoomLevel: 19
            coordinate: actorPosition
            anchorPoint.x: actor_arrow_image.width/2
            anchorPoint.y: actor_arrow_image.height/2+6
            sourceItem: Image{
                id: actor_arrow_image;
                width: 57
                height: 87
                source: 'qrc:/img/actor_arrow.png'
                transform: Rotation {
                    id: actorArrowRotation
                    origin.x: 57/2
                    origin.y: 87/2
                    angle: 0
                    Behavior on angle {
                        SpringAnimation { spring: 2; damping: 0.3; modulus: 360 }
                    }
                }
            }
            z: 11
        }

        MapQuickItem {
            id: grid

            zoomLevel: 19
            anchorPoint.x: imagegrid.width/2
            anchorPoint.y: imagegrid.height/2
            sourceItem: Item {
                Image {
                    id: imagegrid
                    source: 'qrc:/img/net-ok.png'
                    visible: imagegrid_ok
                }
                Image {
                    source: 'qrc:/img/net-err.png'
                    visible: !imagegrid_ok
                }
            }
            visible: false
        }


//        Grid {
//            id: neet
////            coordinate: mapBase.center

//        }

//        MapCircle {
//            center: mapBase.center //QtPositioning.coordinate(50.57195, 36.57219)
//            radius: 64
//            border.width: 3
//            border.color: "#ecff00"
//            opacity: 0.5

//            MouseArea {
//                anchors.fill: parent
//            }
//        }


        MouseArea {
            anchors.fill: parent
            enabled: buildMode
            onMouseXChanged: {
                var pos = mapOverlay.toCoordinate(Qt.point(mouse.x,mouse.y))
                Cyberman.build_check_possibility(pos.latitude, pos.longitude);
            }
            onMouseYChanged: {
                var pos = mapOverlay.toCoordinate(Qt.point(mouse.x,mouse.y))
                Cyberman.build_check_possibility(pos.latitude, pos.longitude);
            }
        }



        // The code below enables SSAA
        layer.enabled: true
        layer.smooth: true
        property int w : mapOverlay.width
        property int h : mapOverlay.height
        property int pr: Screen.devicePixelRatio
        layer.textureSize: Qt.size(w  * 2 * pr, h * 2 * pr)
    }



    Ui {
        id: ui
        anchors.fill: parent
        z: 100
    }

    Compass {
        id: compass
        dataRate: 10
        active: true

        onReadingChanged: {
            var readAz = reading.azimuth;
            actorArrowRotation.angle = readAz;
            actorShadowRotation.angle = readAz;
        }
    }

    Component.onCompleted: {
        Cyberman.actorInit();
        Cyberman.enegry_demo_spawn(actorPosition.latitude, actorPosition.longitude);
    }
}
