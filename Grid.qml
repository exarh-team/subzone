import QtQuick 2.7
import QtLocation 5.9

MapQuickItem {
    id: net
    zoomLevel: 19
    anchorPoint.x: image.width/2
    anchorPoint.y: image.height/2
    sourceItem: Image{
        id: image
        source: 'qrc:/img/net.png'
    }
}
