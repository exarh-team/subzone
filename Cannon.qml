import QtQuick 2.7
import QtLocation 5.9

MapQuickItem {
    property var tiles20
    property int size_x: 1
    property int size_y: 1

    zoomLevel: 19
    sourceItem: Image{
        id: image
        source: 'qrc:/img/cannon.png'
    }
}
